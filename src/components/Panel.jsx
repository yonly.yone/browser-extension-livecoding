import { useEffect, useState } from "react";
import FloatingButton from "./commons/buttons/FloatingButton";

export default function Panel() {
  const [isPanelVisible, setIsPanelVisible] = useState(false);

  const handlePanelToogling = () => setIsPanelVisible((prev) => !prev);
  useEffect(() => {
    console.log("hello from panel useEffect");
  }, []);

  if (!isPanelVisible) {
    return (
      <FloatingButton onClick={handlePanelToogling}>
        Ollassistant
      </FloatingButton>
    );
  }

  return <div> App Panel </div>;
}
