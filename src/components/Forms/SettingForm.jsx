import TextInput from "../commons/inputs/TextInput";

export default function SettingForm() {
  /**
   * HandleFormSubmit
   * @function
   * @param {import("react").FormEvent<SubmitEvent>} event - Submit Event
   */
  const handleSubmit = (event) => {
    event.preventDefault();
  };

  return (
    <form onSubmit={handleSubmit}>
      <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
        <div className="sm:col-span-4">
          <TextInput type="text" label="url du projet" />
        </div>
      </div>
    </form>
  );
}
