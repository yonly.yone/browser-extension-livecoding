/**
 * Button Base Props declaration
 * @typedef {HTMLProps<HTMLInputElement>} BaseButtonProps
 *
 * FloatingButton Custom Props declaration
 * @typedef {object} FloatingButtonCustomProps
 * @property {"primary" | "secondary"} kind - type de bouton
 *
 * FloatingButton Custom Props declaration
 * @typedef {BaseButtonProps & FloatingButtonCustomProps} FloatingButtonProps
 */

import { HTMLProps } from "react";
import { css } from "@emotion/react";
import styled from "@emotion/styled";

/**
 * @param {FloatingButtonCustomProps} props
 */
const StyledFloatingWrapper = styled.div`
  position: fixed;
  top: 50%;
  right: 15px;
  z-index: 9999;
  button {
    background-color: blue;
  }

  ${({ kind }) =>
    kind === "primary" &&
    css`
      color: red;
    `}
`;

/** Floating Button Component
 *@param {FloatingButtonProps} props
 *
 */
export default function FloatingButton({ kind, children, ...props }) {
  return (
    <StyledFloatingWrapper kind={kind}>
      <button {...props}>{children}</button>
    </StyledFloatingWrapper>
  );
}
