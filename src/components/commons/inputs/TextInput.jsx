import { HTMLProps } from "react";

/**
 * TextInput Base Props declaration
 * @typedef {HTMLProps<HTMLInputElement>} BaseInputProps
 *
 * TextInput Custom Props declaration
 * @typedef {object} TextInputCustomProps
 * @property {string} label - Label de l'input
 *
 * TextInput Custom Props declaration
 * @typedef {BaseInputProps & TextInputCustomProps} TextInputProps
 */

/**
 * TextInput Component
 * @param {TextInputProps} props - input props
 */
export default function TextInput({ label, ...props }) {
  return (
    <>
      <label
        for={props.name}
        className="block text-sm font-medium leading-6 text-gray-900"
      >
        {label}
      </label>
      <div className="mt-2">
        <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 sm:max-w-md">
          <input
            {...props}
            class={
              "block flex-1 border-0 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 " +
              props.className
            }
          />
        </div>
      </div>
    </>
  );
}
