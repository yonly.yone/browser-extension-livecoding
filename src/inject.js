import { createRoot } from "react-dom";
import App from "./app";

const root = createRoot(document.getElementById("ollassistant_inject"));

root.render(App());
