chrome.runtime.onInstalled.addListener(() => {
  // Open Setting file on fist install
  chrome.tabs.create({ url: "index.html" });
});

chrome.webNavigation.onCompleted.addListener(async () => {
  //
  chrome.runtime.sendMessage({ type: "navigation_completed" }, () => {
    console.log("message callback");
  });
});

chrome.windows.onFocusChanged.addListener(() => {
  //
  chrome.runtime.sendMessage({ type: "focus_changed" }, () => {
    console.log("message callback");
  });
});

chrome.runtime.onMessage.addListener((message, sender) => {});
