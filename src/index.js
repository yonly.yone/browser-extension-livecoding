import { createRoot } from "react-dom";
import Settings from "./components/Settings";

const root = createRoot(document.getElementById("root"));

root.render(Settings());
