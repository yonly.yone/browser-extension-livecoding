const inject_root = document.createElement("div");
inject_root.id = "ollassistant_inject";
document.body.appendChild(inject_root);

const scriptElem = document.createElement("script");
const injectScript = chrome.runtime.getURL("inject.js");
scriptElem.type = "module";
scriptElem.src = injectScript;
scriptElem.onload = () => {
  chrome.runtime.sendMessage({ type: "script_loaded" }, () => {
    console.log("script loaded");
  });
};
document.head.appendChild(scriptElem);
