import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";

export default defineConfig({
  plugins: [react()],
  publicDir: "./public/",
  build: {
    target: "esnext",
    manifest: true,
    rollupOptions: {
      input: {
        app: "./index.html",
        "service-worker": "./src/service-worker.js",
        "content-script": "./src/content-script.js",
        inject: "./src/inject.js",
      },
      output: {
        entryFileNames: () => "[name].js",
        chunkFileNames: () => "js/[name].js",
        assetFileNames: () => "assets/[name]",
      },
    },
  },
});
